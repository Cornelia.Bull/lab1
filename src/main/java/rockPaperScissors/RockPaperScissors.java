package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Random random_method = new Random();



    public void run() {
        // TODO: Implement Rock Paper Scissors

        // Game loop
        while (true){
            System.out.printf("Let's play round %d\n", roundCounter);

            // Human and Computer choice
            String human_choice = user_choice();
            int index = random_method.nextInt(rpsChoices.size());
            String computer_choice = rpsChoices.get(index);
            String choice_string = String.format("Human chose %s, computer chose %s. ", human_choice, computer_choice);

            // Check who won
            if (is_winner(human_choice, computer_choice)){
                System.out.println(choice_string + " Human wins!");
                humanScore++;
            }
            else if (is_winner(computer_choice, human_choice)){
                System.out.println(choice_string + " Computer wins!");
                computerScore++;
            }
            else{
                System.out.println(choice_string + "It's a tie!");
            }
            System.out.printf("Score: human %s, computer %s\n", humanScore, computerScore);

            String continue_answer = continue_playing();
            if (continue_answer.equals("n")){
                break;
            }
            roundCounter++;
        }
        System.out.println("Bye bye :)");
        
    }

        // is_winner fra Python
        public boolean is_winner(String choice1, String choice2){
            if (choice1.equals("paper")){
                return (choice2.equals("rock"));
            }
            else if (choice1.equals("scissors")){
                return (choice2.equals("paper"));
            }
            else{
                return (choice2.equals("scissors"));
            }
        }

    // user_choice
    public String user_choice (){
        while (true){
            String human_choice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (rpsChoices.contains(human_choice)){
                return human_choice;
            }
            else{
                System.out.println("I don't understand " + human_choice + "Try again");
            }
        }

    }

    // continue_playing
    public String continue_playing(){
        while (true){
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validate_input(continue_answer, Arrays.asList("y", "n"))){
                return (continue_answer);
            }
            else{
                System.out.printf("I do not understand %s. Could you try again?\n", continue_answer);
            }
        }

    }

    // validate_input
    public boolean validate_input(String input, List<String> valid_input){
        input = input.toLowerCase();
        return (valid_input.contains(input));
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
